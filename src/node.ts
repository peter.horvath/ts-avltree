import { CompareFunc } from "./tree";

export class AvlNode<T implements Comparable<T>> {
  public left: AvlNode<T>;
  public right: AvlNode<T>;
  public parent: AvlNode<T>;
  public height: number;
  public size: number;
  public value: T;

  constructor(value: T) {
    this.init(value);
  }

  public init(value: T) {
    this.left = null;
    this.right = null;
    this.parent = null;
    this.height = 1;
    this.size = 1;
    this.value = value;
  }

  //
  // Utility methods
  //

  public isLeftChild(): boolean {
    return (this.parent !== null) && (this === this.parent.left);
  }

  public isRightChild(): boolean {
    return (this.parent !== null) && (this === this.parent.right);
  }

  public leftSize(): number {
    if (this.left === null) {
      return 0;
    } else {
      return this.left.size;
    }
  }

  public rightSize(): number {
    if (this.right === null) {
      return 0;
    } else {
      return this.right.size;
    }
  }

  public leftHeight(): number {
    if (this.left === null) {
      return 0;
    } else {
      return this.left.height;
    }
  }

  public rightHeight(): number {
    if (this.right === null) {
      return 0;
    } else {
      return this.right.height;
    }
  }

  // gives back the smallest node _below_ the node. If there is none,
  // returns nil
  public smallestChild(): AvlNode<T> {
    let n: AvlNode<T> = this;
    if (n.left === null) {
      return null;
    }
    for (;;) {
      n = n.left;
      if (n.left === null) {
        return n;
      }
    }
  }

  // gives back the highest node _below_ the node. If there is none,
  // returns nil
  public highestChild(): AvlNode<T> {
    let n: AvlNode<T> = this;
    if (n.right === null) {
      return null;
    }
    for (;;) {
      n = n.right;
      if (n.right === null) {
        return n;
      }
    }
  }

  //
  // BALANCING
  //

  private fixHs(): boolean {
    let h = this.height;
    let s = this.size;

    let lh = this.leftHeight();
    let rh = this.rightHeight();

    if (lh < rh) {
      this.height = rh + 1;
    } else {
      this.height = lh + 1;
    }

    this.size = this.leftSize() + this.rightSize() + 1;

    return (this.height === h) && (this.size === s);
  }

  public fixHsRec() {
    for (let c: AvlNode<T> = this; c !== null; c = c.parent) {
      if (c.fixHs()) {
        break;
      }
    }
  }

  private rotateLeft() {
    if (this.right === null) {
      console.error("not okay");
    }

    //fmt.Printf("l.h: %d h: %d r.l.h: %d r.h: %d r.r.h: %d\n", n.leftHeight(),
    //  n.height, n.right.leftHeight(), n.right.height, n.right.rightHeight())

    let p = this.parent;
    let b = this.right;
    let c = b.left;

    if (p !== null) {
      if (p.left === this) {
        p.left = b;
      } else {
        p.right = b;
      }
    }

    b.parent = p;
    b.left = this;

    this.parent = b;
    this.right = c;

    if (c != null) {
      c.parent = this;
    }

    this.fixHs();
    b.fixHsRec();
  }

  private rotateRight() {
    if (this.left === null) {
      console.error("not okay #2");
    }

    //fmt.Printf("l.l.h: %d l.h: %d l.r.h: %d h: %d r.h: %d\n",
    //  n.left.leftHeight(), n.left.height, n.left.rightHeight(), n.height, n.rightHeight())

    let p = this.parent;
    let a = this.left;
    let c = a.right;

    if (p != null) {
      if (p.left === this) {
        p.left = a;
    } else {
        p.right = a;
      }
    }

    a.parent = p;
    a.right = this;

    this.parent = a;
    this.left = c;

    if (c != null) {
      c.parent = this;
    }

    this.fixHs();
    a.fixHsRec();
  }

  private balance(): boolean {
    let lh = this.leftHeight();
    let rh = this.rightHeight();

    //fmt.Printf("node: %d, lh: %d, rh: %d, ", n.value.(int), lh, rh)
    if (rh > lh + 1) {
      //fmt.Println("rotateLeft")
      if (this.right.leftHeight() > this.right.rightHeight()) {
        this.right.rotateRight();
      }
      this.rotateLeft();
      return true;
    } else if (lh > rh + 1) {
      //fmt.Println("rotateRight")
      if (this.left.rightHeight() > this.left.leftHeight()) {
        this.left.rotateLeft();
      }
      this.rotateRight();
      return true;
    } else {
      //fmt.Println("no rotate")
      return false;
    }
  }

  public balanceRec() {
    let c: AvlNode<T> = this;
    for (;;) {
      if (c === null) {
        break;
      }
      c.balance();
      c = c.parent;
    }
  }

  public substituteWith(nxt: AvlNode<T>) {
    if (this.parent !== null) {
      if (this.isLeftChild()) {
        this.parent.left = nxt;
      } else {
        this.parent.right = nxt;
      }
    }
    if (this.left !== null) {
      this.left.parent = nxt;
    }
    if (this.right !== null) {
      this.right.parent = nxt;
    }

    nxt.parent = this.parent;
    nxt.left = this.left;
    nxt.right = this.right;
    nxt.size = this.size;
    nxt.height = this.height;

    this.parent = null;
    this.left = null;
    this.right = null;
    this.size = 1;
    this.height = 1;
  }

  //
  // AvlNode methods out of the core logic
  //

  public findChild(v: T, c: CompareFunc<T>): AvlNode<T> {
    let n: AvlNode<T> = this;
    for (;;) {
      if (n === null) {
        return null;
      }
      let r = c(v, n.value);
      if (r < 0) {
        n = n.left;
      } else if (r > 0) {
        n = n.right;
      } else {
        return n;
      }
    }
  }

  public GetNext(): AvlNode<T> {
    let n: AvlNode<T> = this;
    if (n.right !== null) {
      let r = n.right.smallestChild();
      if (r === null) {
        return n.right;
      } else {
        return r;
      }
    } else if (n.parent === null) {
      return null;
    } else {
      let r = n;
      for (;;) {
        if (r.isLeftChild()) {
          return r.parent;
        }
        r = r.parent;
        if (r == null) {
          return null;
        }
      }
    }
  }

  public GetPrev(): AvlNode<T> {
    let n: AvlNode<T> = this;
    if (n.left !== null) {
      let r = n.left.highestChild();
      if (r === null) {
        return n.left;
      } else {
        return r;
      }
    } else if (n.parent === null) {
      return null;
    } else {
      let r = n;
      for (;;) {
        if (r.isLeftChild()) {
          return r.parent;
        }
        r = r.parent;
        if (r === null) {
          return null;
        }
      }
    }
  }

  public GetValue(): T {
    return this.value;
  }

  public GetIdx(): number {
    let n: AvlNode<T> = this;
    let r = n.leftSize() + 1;
    for (;;) {
      if (n.isRightChild()) {
        r += n.parent.leftSize() + 1;
      }
      n = n.parent;
      if (n === null) {
        break;
      }
    }
    return r;
  }

  public getChildByIdx(n: number): AvlNode<T> {
    if ((n < 0) || (n >= this.size)) {
      return null;
    } else if (n < this.leftSize()) {
      return this.left.getChildByIdx(n);
    } else if (n === this.leftSize()) {
      return this;
    } else {
      return this.right.getChildByIdx(n - this.leftSize() - 1);
    }
  }

  //
  // Unsorted avltree functionality
  //
  public UnsortedSetValue(v: T): AvlNode<T> {
    this.value = v;
    return this;
  }
}
