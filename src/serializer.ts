  package avltree
  
  import (
    "strconv"
    "encoding/json"
  )
  
  type Serializer func (a interface{}) string
  type Unserializer func (a interface{}) interface{}
  
  func (n *AvlNode) ToStringFn(ser Serializer) string {
    res := "{"
    ex := false
    if n.left != nil {
      res += "\"left\":" + n.left.ToStringFn(ser)
      ex = true
    }
    if ex {
      res += ","
    }
    res += "\"size\":" + strconv.FormatInt(n.size, 10) + ",\"height\":" + strconv.Itoa(int(n.height))
    if ser != nil {
      res += ",\"value\":" + ser(n.value)
    }
    if n.right != nil {
      res += ",\"right\":" + n.right.ToStringFn(ser)
    }
    res += "}"
    return res
  }
  
  func (n *AvlNode) ToString() string {
    return n.ToStringFn(nil)
  }
  
  func (t *AvlTree) ToStringFn(ser Serializer) string {
    if t.root == nil {
      return "{}"
    } else {
      return t.root.ToStringFn(ser)
    }
  }
  
  func (t *AvlTree) ToString() string {
    return t.ToStringFn(nil)
  }
  
  func convertStringMapToAvlNode(m map[string]interface{}, unser Unserializer) *AvlNode {
    n := new(AvlNode)
    n.height = byte(m["height"].(float64))
    n.size = int64(m["size"].(float64))
  
    if _, ok := m["left"]; ok {
      n.left = convertStringMapToAvlNode(m["left"].(map[string]interface{}), unser)
      n.left.parent = n
    }
  
    if _, ok := m["right"]; ok {
      n.right = convertStringMapToAvlNode(m["right"].(map[string]interface{}), unser)
      n.right.parent = n
    }
  
    if _, ok := m["value"]; ok {
      n.value = unser(m["value"])
    }
  
    return n
  }
  
  func ConvertStringToAvlTree(s string, unser Unserializer) *AvlTree {
    var stuff map[string]interface{}
    json.Unmarshal([]byte(s), &stuff)
  
    t := NewAvlTree(nil, 0)
  
    t.root = convertStringMapToAvlNode(stuff, unser)
    return t
  }
