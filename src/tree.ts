export const AllowDuplicates : number = 1;

import { AvlNode } from "./node";

export type CompareFunc<T> = (a: T, b: T) => number;

export class AvlTree<T implements Comparable<T>> {
  private root: AvlNode<T>;
  private compare: CompareFunc<T>;
  private flags: number;

  constructor(flags: number, compare: CompareFunc<T>) {
    this.Init(flags, compare);
  }

  public Init(flags: number, compare: CompareFunc<T>) {
    this.root = null;
    this.compare = compare;
    this.flags = flags;
  }

  private add(v: T): AvlNode<T> {
    if (this.root === null) {
      let n = new AvlNode(v);
      this.root = n;
      return n;
    }

    let toLeft: boolean;
    let n = this.root;
    for (;;) {
      let cr = this.compare(v, n.value);
      if (cr < 0) {
        toLeft = true;
      } else if (cr > 0) {
        toLeft = false;
      } else {
        /* tslint:disable-next-line */
        if ((this.flags & AllowDuplicates) === 0) {
          console.error("no dupes");
          /*
          ret := n.value
          n.value = v
          return ret
          */
        } else {
          toLeft = n.leftSize() < n.rightSize();
        }
      }

      if (toLeft) {
        if (n.left === null) {
          this.addLeafLeft(n, new AvlNode(v));
          return null;
        } else {
          n = n.left;
        }
      } else {
        if (n.right === null) {
          this.addLeafRight(n, new AvlNode(v));
          return null;
        } else {
          n = n.right;
        }
      }
    }
  }

  private detachNode(n: AvlNode<T>) {
    if ((n.left === null) && (n.right === null)) {
      if (n.parent !== null) {
        if (n.isLeftChild()) {
          n.parent.left = null;
        } else {
          n.parent.right = null;
        }
        n.parent.fixHsRec();
        n.parent.balanceRec();
        this.fixRoot(n.parent);
      } else {
        this.root = null;
      }
    } else if ((n.left === null) && (n.right != null)) {
      if (n.parent === null) {
        this.root = n.right;
        n.right.parent = null;
      } else {
        if (n.isLeftChild()) {
          n.parent.left = n.right;
        } else {
          n.parent.right = n.right;
        }
        n.right.parent = n.parent;
        n.parent.fixHsRec();
        n.parent.balanceRec();
        this.fixRoot(n.parent);
      }
    } else if ((n.left != null) && (n.right == null)) {
      if (n.parent === null) {
        this.root = n.left;
        n.left.parent = null;
      } else {
        if (n.isLeftChild()) {
          n.parent.left = n.left;
        } else {
          n.parent.right = n.left;
        }
        n.left.parent = n.parent;
        n.parent.fixHsRec();
        n.parent.balanceRec();
        this.fixRoot(n.parent);
      }
    } else {
      let ls = n.left.size;
      let rs = n.right.size;
      let nxt: AvlNode<T>;
      let nextRootFrom: AvlNode<T>;
      if (ls < rs) {
        nextRootFrom = n.left;
        nxt = n.right;
        if (nxt.left !== null) {
          nxt = nxt.smallestChild();
        }
      } else {
        nextRootFrom = n.right;
        nxt = n.left;
        if (nxt.right !== null) {
          nxt = nxt.highestChild();
        }
      }
      this.detachNode(nxt);
      n.substituteWith(nxt);
      this.fixRoot(nextRootFrom);
    }

    n.parent = null;
    n.right = null;
    n.left = null;
    n.size = 1;
    n.height = 1;
  }

  // can be called **only** if to.left == nil !
  public addLeafLeft(to: AvlNode<T>, what: AvlNode<T>) {
    to.left = what;
    what.parent = to;
    to.fixHsRec();
    to.balanceRec();
    this.fixRoot(to);
  }

  // can be called **only** if to.right == nil !
  public addLeafRight(to: AvlNode<T>, what: AvlNode<T>) {
    to.right = what;
    what.parent = to;
    to.fixHsRec();
    to.balanceRec();
    this.fixRoot(to);
  }

  public fixRoot(n: AvlNode<T>) {
    for (;;) {
      if (n.parent === null) {
        break;
      } else {
        n = n.parent;
      }
    }
    this.root = n;
  }

  //
  // AvlTree methods out of the core logic
  //

  // Clear removes all elements from the tree, keeping the
  // current options and compare function
  public Clear() {
    this.Init(this.flags, this.compare);
  }

  // Size returns the number of elements in the tree
  public GetSize(): number {
    if (this.root === null) {
      return 0;
    } else {
      return this.root.size;
    }
  }

  public GetComparator(): CompareFunc<T> {
    return this.compare;
  }

  public SetComparator(c: CompareFunc<T> ) {
    if (this.GetSize() > 1) {
      console.error("comparators can be changed only for tree sizes <= 1");
    } else {
      this.compare = c;
    }
  }

  public HasByCompareFunc(v: T, c: CompareFunc<T>): boolean {
    if (this.root === null) {
      return false;
    } else {
      return this.root.findChild(v, c) != null;
    }
  }

  public Has(v: T): boolean {
    return this.HasByCompareFunc(v, this.compare);
  }

  public FindByCompareFunc(p: T, c: CompareFunc<T>): AvlNode<T> {
    if (this.root === null) {
      return null;
    } else {
      return this.root.findChild(p, c);
    }
  }

  public Find(p: T): AvlNode<T> {
    return this.FindByCompareFunc(p, this.compare);
  }

  public GetFirst(): AvlNode<T> {
    if (this.root === null) {
      return null;
    } else if (this.root.left === null) {
      return this.root;
    } else {
      return this.root.smallestChild();
    }
  }

  public GetLast(): AvlNode<T> {
    if (this.root === null) {
      return null;
    } else if (this.root.right === null) {
      return this.root;
    } else {
      return this.root.highestChild();
    }
  }

  public IsEmpty(): boolean {
    return this.root == null;
  }

  public Remove(v: T): boolean {
    if (this.root === null) {
      return false;
    } else {
      let n = this.root.findChild(v, this.compare);
      if (n === null) {
        return false;
      } else {
        this.detachNode(n);
        return true;
      }
    }
  }

  public RemoveNode(n: AvlNode<T>) {
    this.detachNode(n);
  }

  public NodeAt(n: number): AvlNode<T> {
    if (this.root === null) {
      return null;
    } else {
      return this.root.getChildByIdx(n);
    }
  }

  public At(n: number): T {
    let node = this.NodeAt(n);
    if (node === null) {
      return null;
    } else {
      return node.GetValue();
    }
  }

  //
  // Unsorted avltree functionality
  //
  public UnsortedAddFirst(v: T): AvlNode<T> {
    let newN = new AvlNode(v);
    if (this.root === null) {
      this.root = newN;
    } else if (this.root.left === null) {
      this.addLeafLeft(this.root, newN);
    } else {
      this.addLeafLeft(this.root.smallestChild(), newN);
    }
    return newN;
  }

  public UnsortedAddLast(v: T): AvlNode<T> {
    let newN = new AvlNode(v);
    if (this.root === null) {
      this.root = newN;
    } else if (this.root.right === null) {
      this.addLeafRight(this.root, newN);
    } else {
      this.addLeafRight(this.root.highestChild(), newN);
    }
    return newN;
  }

  public UnsortedAddAfter(n: AvlNode<T>, v: T): AvlNode<T> {
    let newN = new AvlNode(v);
    if (n.right === null) {
      this.addLeafRight(n, newN);
    } else if (n.right.left === null) {
      this.addLeafLeft(n.right, newN);
    } else {
      this.addLeafLeft(n.right.smallestChild(), newN);
    }
    return newN;
  }

  public UnsortedAddBefore(n: AvlNode<T>, v: T): AvlNode<T> {
    let newN = new AvlNode(v);
    if (n.left === null) {
      this.addLeafLeft(n, newN);
    } else if (n.left.right === null) {
      this.addLeafRight(n.left, newN);
    } else {
      this.addLeafRight(n.left.smallestChild(), newN);
    }
    return newN;
  }

  public UnsortedInsert(idx: number, v: T): AvlNode<T> {
    if (idx < 0) {
      console.error("negative idx is illegal");
      return null;
    } else if (idx === 0) {
      return this.UnsortedAddFirst(v);
    } else if (idx <= this.GetSize()) {
      return this.UnsortedAddAfter(this.NodeAt(idx - 1), v);
    } else {
      console.error ("idx > size is illegal");
      return null;
    }
  }
}
