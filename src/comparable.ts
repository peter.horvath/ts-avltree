export interface Comparable<T> {
  public compareTo(t T): number;
}
