module.exports = function(grunt) {
  'use strict';

  grunt.initConfig({
    pkg : grunt.file.readJSON('package.json'),

    jshint : {
      options : {
        jshintrc : 'jshint.json'
      },
      dev : {
        src : ['Gruntfile.js']
      }
    },

    tslint : {
      options : {
        configuration : "tslint.json",
        force : false,
        fix : false
      },
      files : {
        src : ["src/**/*.ts", "test/**/*.ts"]
      }
    },

    mkdir : {
      dist : {
        options : {
          create : ['dist']
        }
      },
    },

    tsc : {
      dist : {
        options : {
          args : [ "-p", "tsconfig-dist.json" ],
          debug : true
        }
      },
      test : {
        options : {
          args : [ "-p", "tsconfig-test.json" ],
          debug : true
        }
      }
    },

    clean : ['dist']
  });

  grunt.loadNpmTasks("grunt-contrib-clean");
  grunt.loadNpmTasks("grunt-contrib-jshint");
  grunt.loadNpmTasks("grunt-mkdir");
  grunt.loadNpmTasks("grunt-tslint");
  grunt.loadNpmTasks("grunt-direct-tsc");

  grunt.registerTask("check", ["tslint", "jshint"]);
  grunt.registerTask("dist", ["check", "mkdir:dist", "tsc:dist"]);
  grunt.registerTask("default", ["dist"]);
  grunt.registerTask("test", ["check", "mkdir:dist", "tsc:test"]);
};
