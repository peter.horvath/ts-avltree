// trivial testcases

package avltree

import (
  "fmt"

  "testing"
)

func noTestAvlTree1(test *testing.T) {
  tree := NewAvlTree(compare_int64_int64, 0)
  fmt.Println("empty tree:")
  tree.Dump()

  /*
  tree.Add(primitive.NewInt32(-2))
  tree.Dump()
  tree.Add(primitive.NewInt32(-1))
  tree.Dump()
  */

  /*
  tree.root.rotateLeft()
  tree.fixRoot(tree.root)
  */

  for n := 0; n < 10; n++ {
    tree.Add(n)
    fmt.Printf("added %d:\n", n)
    tree.Dump()
  }

  for n := int64(0); n < 10; n++ {
    fmt.Printf("value at %d: %d\n", n, tree.At(n).(int64))
  }

  fmt.Println("print ordered")
  avlNode := tree.GetFirst()
  for {
    if avlNode == nil {
      break
    }
    nxt := avlNode.GetValue()
    fmt.Printf("%p\n", nxt)
    fmt.Printf("%d\n", nxt.(int64))
    avlNode = avlNode.GetNext()
  }

  fmt.Println("rewind")

  for n := 0; n < 10; n++ {
    tree.Remove(n)
    fmt.Printf("removed %d:\n", n)
    tree.Dump()
  }
}

// TODO: test cleanup

func unserializeint(stuff interface{}) interface{} {
  return int64(stuff.(float64))
}

var s string = `{
  "left": {
    "left": {
      "size": 1,
      "height": 1,
      "value": 2
    },
    "size": 3,
    "height": 2,
    "value": 5,
    "right": {
      "size": 1,
      "height": 1,
      "value": 7
    }
  },
  "size": 6,
  "height": 3,
  "value": 8,
  "right": {
    "left": {
      "size": 1,
      "height": 1,
      "value": 9
    },
    "size": 2,
    "height": 2,
    "value": 13
  }
}`

func noTestSerialize(test *testing.T) {
  t := ConvertStringToAvlTree(s, unserializeint)
  t.compare = compare_int64_int64
  t.Dump()
}

func noTestCantRemove5(test *testing.T) {
  t := ConvertStringToAvlTree(s, unserializeint)
  t.compare = compare_int64_int64
  t.Dump()
  t.Remove(5)
  t.Dump()
}
