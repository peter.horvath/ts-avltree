// crashtest

require('seedrandom');

const verbose = false;

function panic(msg: string) {
  console.error(msg);
  process.exit(1);
}

function foundPath(n: number, path: string, pathes []string) {
  if (pathes[n] !== "") {
    panic("node found on multiple pathes: first on " + pathes[n] + ", now on " + path);
  } else {
    pathes[n] = path;
  }
}

function pathCheck(n: AvlNode, pathes: []string) {
  if (n.parent === null) {
    foundPath(n.value, "/" + n.value, 10), pathes);
  } else {
    foundPath(n.value, pathes[n.parent.value] + "/" + n.value, pathes);
  }
  if (n.left !== null) {
    pathCheck(n.left, pathes);
  }
  if (n.right !== null) {
    pathCheck(n.right, pathes);
  }
}

function pathesDump(pathes []string) {
  for n, path := range pathes {
    fmt.Printf("%d: %s\n", n, path)
  }
}

function check(n: AvlNode) {
  if (n.parent !== null) {
    if (n.parent.left !== n) && (n.parent.right !== n) {
      panicNode(n, "parent exists, but this node is neither left neither right child")
    }
    if (n.parent.left == n) && (n.parent.right == n) {
      panicNode(n, "node is both left and right child")
    }
  }
  var estSize int64 = 1
  var estHeight byte = 1
  if n.left != nil {
    estSize += n.left.size
    estHeight = n.left.height + 1
    if n.left.parent != n {
      panicNode(n, "n.left.parent != n")
    }
    n.left.check()
  }
  if n.right != nil {
    estSize += n.right.size
    if estHeight < n.right.height + 1 {
      estHeight = n.right.height + 1
    }
    if n.right.parent != n {
      panicNode(n, "n.right.parent != n")
    }
    n.right.check()
  }
  if n.size != estSize {
    panicNode(n, "invalid node size")
  }
  if n.height != estHeight {
    panicNode(n, "invalid node height")
  }
  if (n.leftHeight() > n.rightHeight() + 1) || (n.rightHeight() > n.leftHeight() + 1) {
    panicNode(n, "left and right heights are too different, not enough balance")
  }
  if n.value == nil {
    panicNode(n, "n.value == nil is possible but not in this test case")
  }
}

func (t *AvlTree)check(size int64) {
  if t.compare == nil {
    panic("t.compare == nil is possible but not in this test case")
  }
  if t.root != nil {
    if t.root.parent != nil {
      panic("t.root.parent != nil")
    }
    pathes := make([]string, size)
    t.root.pathCheck(pathes)
    if verbose {
      pathesDump(pathes)
      t.Dump()
    }
    t.root.check()
  }
}

function testForSize(size: number) {
  console.log("pseudorandom test for size " + size);

  let tree = new AvlTree(compare_int64_int64, 0);

  Math.seedrandom('42');

  ba := make([]bool, size)

  testAdd := func(n int64) {
    if tree.Has(n) {
      panic("is there, but it should not")
    }
    if verbose {
      fmt.Println("is not there, addition")
    }
    tree.Add(n)
    ba[n] = true
  }

  testRemoval := func(n int64) {
    if !tree.Has(n) {
      panic("is not there, but it should")
    }
    if verbose {
      fmt.Println("is there, removal")
    }
    tree.Remove(n)
    ba[n] = false
  }

  for a := int64(0); a < size * size; a++ {
    tree.check(size)
    n := int64(rand.Intn(int(size)))
    if verbose {
      fmt.Printf("%d:\n", n)
    }
    if ba[n] {
      testRemoval(n)
    } else {
      testAdd(n)
    }
  }

  fmt.Println("Endphase")
  for {
    if tree.IsEmpty() {
      break
    }
    tree.check(size)
    n := int64(rand.Intn(int(size)))
    for {
      if ba[n] {
        if verbose {
          fmt.Printf("%d:\n", n)
        }
        testRemoval(n)
        break
      }
      n++
      if n == size {
        n = 0
      }
    }
  }
}

export function TestCrash() {
  for (let size: number = 2; size < 256; size++) {
    testForSize(size)
  }
}
